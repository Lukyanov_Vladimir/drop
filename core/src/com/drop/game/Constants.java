package com.drop.game;

public class Constants {
    public static final String APP_TITLE = "Drop";
    public static final int APP_WIDTH = 800;
    public static final int APP_HEIGHT = 480;
}
