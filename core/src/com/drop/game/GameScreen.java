package com.drop.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

public class GameScreen implements Screen {
    private final Drop GAME;
    private final int BUCKET_WIDTH = 64;
    private final int BUCKET_HEIGHT = 64;
    private final int RAINDROP_WIDTH = 64;
    private final int RAINDROP_HEIGHT = 64;
    private final int SPEED_SPAWN_RAINDROPS = 1000;
    private final int SPEED_RAINDROPS = 200;
    private final int SPEED_BUCKET= 400;

    private Texture dropletTexture;
    private Texture bucketTexture;
    private Sound dropSound;
    private OrthographicCamera camera;
    private Rectangle bucket;
    private Vector3 touchPos;
    private Array<Rectangle> raindrops;
    private long lastDropTime;
    private int dropsGathered;

    public GameScreen(final Drop GAME) {
        this.GAME = GAME;

        dropletTexture = new Texture(Gdx.files.internal("droplet.png"));
        bucketTexture = new Texture(Gdx.files.internal("bucket.png"));


        dropSound = Gdx.audio.newSound(Gdx.files.internal("waterdrop.mp3"));

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Constants.APP_WIDTH, Constants.APP_HEIGHT);

        bucket = new Rectangle();
        bucket.x = Constants.APP_WIDTH / 2 - BUCKET_WIDTH / 2;
        bucket.y = 20;
        bucket.width = BUCKET_WIDTH;
        bucket.height = BUCKET_HEIGHT;

        touchPos = new Vector3();

        raindrops = new Array<Rectangle>();

        spawnRaindrop();
    }

    private void spawnRaindrop() {
        Rectangle raindrop = new Rectangle();
        raindrop.x = MathUtils.random(0, Constants.APP_WIDTH - RAINDROP_WIDTH);
        raindrop.y = Constants.APP_HEIGHT;
        raindrop.width = RAINDROP_WIDTH;
        raindrop.height = RAINDROP_HEIGHT;
        raindrops.add(raindrop);

        lastDropTime = TimeUtils.millis();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        GAME.batch.setProjectionMatrix(camera.combined);

        GAME.batch.begin();

        GAME.font.draw(GAME.batch, "Drops collected: " + dropsGathered, 0, Constants.APP_HEIGHT);
        GAME.batch.draw(bucketTexture, bucket.x, bucket.y);

        for (Rectangle raindrop : raindrops) {
            GAME.batch.draw(dropletTexture, raindrop.x, raindrop.y);
        }

        GAME.batch.end();

        if (Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.x = (int) (touchPos.x - BUCKET_WIDTH / 2);
        }


        if (bucket.x < 0) bucket.x = 0;
        if (bucket.x > Constants.APP_WIDTH - BUCKET_WIDTH)
            bucket.x = Constants.APP_WIDTH - BUCKET_WIDTH;
        if (bucket.y > Constants.APP_HEIGHT - BUCKET_HEIGHT)
            bucket.y = Constants.APP_HEIGHT - BUCKET_HEIGHT;
        if (bucket.y < 0) bucket.y = 0;

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
            bucket.x -= SPEED_BUCKET * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            bucket.x += SPEED_BUCKET * Gdx.graphics.getDeltaTime();


        if (TimeUtils.millis() - lastDropTime > SPEED_SPAWN_RAINDROPS) spawnRaindrop();

        Iterator<Rectangle> iterator = raindrops.iterator();
        while (iterator.hasNext()) {
            Rectangle raindrop = iterator.next();
            raindrop.y -= SPEED_RAINDROPS * Gdx.graphics.getDeltaTime();
            if (raindrop.y + RAINDROP_HEIGHT < 0) iterator.remove();
            if (raindrop.overlaps(bucket)) {
                dropsGathered++;
                dropSound.play();
                iterator.remove();
            }
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        dropSound.dispose();
        dropletTexture.dispose();
        bucketTexture.dispose();
    }
}
